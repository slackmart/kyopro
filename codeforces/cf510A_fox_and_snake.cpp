// Martin
// https://codeforces.com/problemset/problem/510/A
// implementation

#include<iostream>

using namespace std;

int main() {
    int n, m;
    cin >> n >> m;

    bool start = false;
    for (int i = 0; i < n; i++) {

        for (int j = 0; j < m; j++) {
            if (i & 1) {
                if (j == (m - 1)) {
                    if (start) cout << '.';
                    else cout << '#';
                } else if (j == 0) {
                    if (start) cout << '#';
                    else cout << '.';
                } else {
                    cout << '.';
                }
            } else {
                cout << '#';
            }
        }

        if (i & 1) start = !start;
        cout << endl;
    }

    return 0;
}
