#include<iostream>

using namespace std;

int main() {
    int n, m;
    cin >> n >> m;

    char c;
    int scores[n][m];

    getchar();  // read new line

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            scanf("%c", &c);
            scores[i][j] = (int)c - '0';
        }
        getchar();  // read new line
    }

    int answer = 0;
    for (int i = 0; i < n; i++) {
        bool was_best = false;
        for (int j = 0; j < m; j++) {
            bool is_best = true;
            for (int k = 0; k < n; k++) {
                if (scores[k][j] > scores[i][j]) is_best = false;
            }
            if (is_best) was_best = true;
        }
        if (was_best) answer++;
    }
    cout << answer << endl;

    return 0;
}
